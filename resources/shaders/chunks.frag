#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(early_fragment_tests) in;

layout(location = 0) in vec2 frag_tex;
layout(location = 1) in float frag_ao;

layout(location = 0) out vec4 color;

layout(set = 1, binding = 1) uniform texture2D colormap;
layout(set = 1, binding = 2) uniform sampler colorsampler;

void main() {
    color = texture(sampler2D(colormap, colorsampler), frag_tex);
    color.rgb = mix(color.rgb, vec3(0.0, 0.0, 0.0), 0.3 * (3 - frag_ao));
}
