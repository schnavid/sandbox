#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex_coord;
layout(location = 3) in vec2 tile_uv;
layout(location = 4) in float ambient_occlusion;

layout(location = 0) out vec2 frag_tex;
layout(location = 1) out float frag_ao;

layout(std140, set = 0, binding = 0) uniform Args {
    mat4 world;
    mat4 view;
    mat4 proj;
};

void main() {
    frag_ao = ambient_occlusion;
    frag_tex = tex_coord;
    gl_Position = proj * view * world * vec4(position, 1.0);
}
