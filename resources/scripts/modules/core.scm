(define-texture core stone "core/stone.png")
(define-texture core dirt  "core/dirt.png")
(define-texture core grass-side "core/grass-side.png")
(define-texture core grass-top "core/grass-top.png")

(define-block core nothing
              (textures-nothing)
              nothing
              #f
              #f)

(define-block core stone
              (textures-all tex:core:stone)
              stone
              #t
              #f)

(define-block core dirt
              (textures-all tex:core:dirt)
              dirt
              #t
              #f)

(define-block core grass
              (textures-pillar tex:core:grass-side tex:core:grass-top tex:core:dirt)
              grass
              #t
              #f)

(define-class <test-entity> (<block-entity>))

(define-method (update (obj <test-entity>))
  1)

(define-block core entity-test
  (textures-all tex:core:stone)
  stone
  #t
  <test-entity>)

