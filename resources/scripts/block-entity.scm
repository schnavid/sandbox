(use-modules (oop goops))

(define-class <block-entity> ()
  position)

(define-method (update (obj <block-entity>)) (0))
(define-method (on-activate (obj <block-entity>) block-position face) (#t))
