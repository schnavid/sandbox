(define-syntax t-id
  (syntax-rules ()
    ((t-id module name)
     (texture-id
       (symbol->string (quote module))
       (symbol->string (quote name))))))

(define-macro
  (define-texture
    module
    name
    path)
  `(define ,(string->symbol
             (string-append
               "tex:"
               (symbol->string module)
               ":"
               (symbol->string name)))
    (register-texture
      ,(symbol->string module)
      ,(symbol->string name)
      ,path)))

(define-macro
  (define-block
    module
    name
    textures
    material
    opaque
    block-entity-class)
  `(define ,(string->symbol
             (string-append
               "blk:"
               (symbol->string module)
               ":"
               (symbol->string name)))
     (register-block
       ,(symbol->string module)
       ,(symbol->string name)
       ,textures
       ,(symbol->string material)
       ,opaque
       ,block-entity-class)))
