use guile_scheme::SCM;
use serde::export::fmt::{Debug, Error};
use serde::export::Formatter;

#[derive(Copy, Clone)]
pub struct BlockEntity {
    pub(crate) object: SCM,
}

impl Debug for BlockEntity {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        write!(f, "BlockEntity {{ {:?} }}", self.object)
    }
}

impl BlockEntity {
    pub fn new(class: SCM) -> BlockEntity {
        BlockEntity {
            object: SCM::make(SCM::list_1(class)),
        }
    }
}
