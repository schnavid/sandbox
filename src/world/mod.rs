use crate::world::entity::BlockEntity;
use crate::world::registry::BLOCK_REGISTRY;
use guile_scheme::SCM;
use nalgebra::Point3;
use noise::{NoiseFn, Perlin};
use std::{collections::HashMap, fmt::Debug};

pub mod entity;
pub mod registry;

#[derive(Debug)]
pub enum WorldError {
    BlockNotRegistered(BlockId),
    BlockCoordinatesOutOfBounds,
    /// There was an attempt to read or manipulate data of a chunk, that's not loaded
    ChunkNotLoaded,
}

pub type BlockId = u32;
pub type BlockPosition = Point3<u8>;

#[derive(Clone)]
pub struct Blocks(pub Box<[BlockId]>); // len = 4096 = 16 * 16 * 16

impl Debug for Blocks {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "<blocks>")
    }
}

#[derive(Debug, Clone)]
pub struct Chunk {
    pub blocks: Blocks,
    pub block_entities: HashMap<BlockPosition, BlockEntity>,
}

impl Chunk {
    pub fn update(&mut self) {
        let update = SCM::lookup("update").variable_ref();

        self.block_entities.iter().for_each(|(_, entity)| {
            update.call(&[entity.object]);
        });
    }

    pub fn generate(perlin: &Perlin, x: i32, y: i32, z: i32) -> Chunk {
        let registry = BLOCK_REGISTRY.lock().unwrap();

        let nothing_id = registry.get_id("core", "nothing").unwrap();
        let stone_id = registry.get_id("core", "stone").unwrap();
        let dirt_id = registry.get_id("core", "dirt").unwrap();
        let grass_id = registry.get_id("core", "grass").unwrap();

        let mut blocks = vec![nothing_id; 4096];

        for bx in 0..16 {
            for bz in 0..16 {
                let h = perlin.get([
                    (x as f64) + (bx as f64) / 16.0,
                    (z as f64) + (bz as f64) / 16.0,
                ]) / 2.0
                    + 0.5;
                let h = (h * 16.0) as usize;

                for by in (y as usize * 16)..h {
                    if by == (y as usize * 16) + 15 {
                        break;
                    }

                    if h - by == 1 {
                        blocks[bz * 256 + by * 16 + bx] = grass_id;
                    } else if h - by <= 3 {
                        blocks[bz * 256 + by * 16 + bx] = dirt_id;
                    } else {
                        blocks[bz * 256 + by * 16 + bx] = stone_id;
                    }
                }
            }
        }

        Chunk {
            blocks: Blocks(blocks.into_boxed_slice()),
            block_entities: Default::default(),
        }
    }

    pub fn empty() -> Chunk {
        let blocks = vec![0; 4096];

        Chunk {
            blocks: Blocks(blocks.into_boxed_slice()),
            block_entities: Default::default(),
        }
    }
}

impl std::ops::Index<(usize, usize, usize)> for Chunk {
    type Output = BlockId;

    fn index(&self, (x, y, z): (usize, usize, usize)) -> &Self::Output {
        &self.blocks.0[z * 256 + y * 16 + x]
    }
}

// Region =
// 32 * 32 Chunk Column =
// 16 Chunk =
// 16 * 16 * 16 Block

#[derive(Debug)]
pub struct ChunkColumn(Box<[Chunk]>); // len = 16

#[derive(Debug)]
pub struct Region(Box<[ChunkColumn]>); // len = 1024 = 32 * 32

impl Region {
    pub fn empty() -> Region {
        Region(
            (0..1024)
                .map(|_| {
                    ChunkColumn(
                        (0..16)
                            .map(|_| Chunk::empty())
                            .collect::<Vec<_>>()
                            .into_boxed_slice(),
                    )
                })
                .collect::<Vec<_>>()
                .into_boxed_slice(),
        )
    }
}

pub fn chunk_to_length_encoding(ch: &Chunk) -> Vec<(BlockId, u16)> {
    let mut result: Vec<(BlockId, u16)> = Vec::new();
    for b in ch.blocks.0.iter() {
        if result.is_empty() || result.last().unwrap().0 != *b {
            result.push((*b, 1));
        } else {
            result.last_mut().unwrap().1 += 1;
        }
    }
    result
}

#[derive(Debug)]
pub struct World {
    pub regions: HashMap<(i32, i32), Region>,
    pub perlin: Perlin,
}

impl Default for World {
    fn default() -> Self {
        World::new()
    }
}

impl World {
    pub fn new() -> World {
        World {
            regions: HashMap::new(),
            perlin: Perlin::new(),
        }
    }

    pub fn get_block(&self, x: i32, y: i32, z: i32) -> Option<BlockId> {
        let chunk = self.get(
            (x as f32 / 16.0).floor() as i32,
            (y as f32 / 16.0).floor() as i32,
            (z as f32 / 16.0).floor() as i32,
        )?;
        Some(chunk[((x % 16) as usize, (y % 16) as usize, (z % 16) as usize)])
    }

    pub fn get(&self, x: i32, y: i32, z: i32) -> Option<&Chunk> {
        Some(
            &self
                .regions
                .get(&(
                    (x as f32 / 32.0).floor() as i32,
                    (z as f32 / 32.0).floor() as i32,
                ))?
                .0[((((z % 32) + 32) % 32) * 32 + (((x % 32) + 32) % 32)) as usize]
                .0[y as usize],
        )
    }

    pub fn get_mut(&mut self, x: i32, y: i32, z: i32) -> Option<&mut Chunk> {
        Some(
            &mut self.regions.get_mut(&(x / 32, z / 32))?.0
                [((((z % 32) + 32) % 32) * 32 + (((x % 32) + 32) % 32)) as usize]
                .0[y as usize],
        )
    }

    pub fn set(&mut self, x: i32, y: i32, z: i32, chunk: Chunk) {
        self.regions
            .entry((x / 32, z / 32))
            .or_insert_with(Region::empty)
            .0[((((z % 32) + 32) % 32) * 32 + (((x % 32) + 32) % 32)) as usize]
            .0[y as usize] = chunk;
    }

    pub fn generate_region(&mut self, x: i32, z: i32) {
        self.regions.insert(
            (x, z),
            Region(
                (0..1024)
                    .map(|i| generate_chunk_column(&self.perlin, x * 32 + i % 32, z * 32 + i / 32))
                    .collect::<Vec<_>>()
                    .into_boxed_slice(),
            ),
        );
    }

    pub fn update(&mut self) {
        self.regions.iter_mut().for_each(|(_, r)| {
            r.0.iter_mut()
                .for_each(|c| c.0.iter_mut().for_each(|chunk| chunk.update()))
        })
    }
}

pub fn generate_chunk_column(perlin: &Perlin, x: i32, z: i32) -> ChunkColumn {
    ChunkColumn(
        (0..16)
            .map(|y| Chunk::generate(perlin, x, y, z))
            .collect::<Vec<_>>()
            .into_boxed_slice(),
    )
}
