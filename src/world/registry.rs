use crate::graphics::registry::{TextureInfo, TextureRegistry};
use crate::{
    graphics::registry::{TextureId, TEXTURE_REGISTRY},
    world::BlockId,
};
use guile_scheme::SCM;
use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum BlockMaterial {
    Nothing,
    Stone,
    Wood,
    Dirt,
    Grass,
}

impl From<u8> for BlockMaterial {
    fn from(n: u8) -> Self {
        match n {
            1 => BlockMaterial::Stone,
            2 => BlockMaterial::Wood,
            3 => BlockMaterial::Dirt,
            4 => BlockMaterial::Grass,
            _ => BlockMaterial::Nothing,
        }
    }
}

impl Into<u8> for BlockMaterial {
    fn into(self) -> u8 {
        match self {
            BlockMaterial::Nothing => 0,
            BlockMaterial::Stone => 1,
            BlockMaterial::Wood => 2,
            BlockMaterial::Dirt => 3,
            BlockMaterial::Grass => 4,
        }
    }
}

#[derive(Debug, Clone)]
pub enum BlockTexturing {
    All(TextureId),
    Pillar {
        top: TextureId,
        side: TextureId,
        bottom: TextureId,
    },
    None,
}

impl BlockTexturing {
    fn get_info(&self, block_side: u8, reg: &TextureRegistry) -> TextureInfo {
        match self {
            BlockTexturing::All(id) => reg.get(*id).unwrap(),
            BlockTexturing::Pillar { top, side, bottom } => match block_side {
                4 => reg.get(*top).unwrap(),
                5 => reg.get(*bottom).unwrap(),
                _ => reg.get(*side).unwrap(),
            },
            BlockTexturing::None => unreachable!(),
        }
    }

    pub fn get_tex_coords(&self, block_side: u8) -> [[f32; 2]; 4] {
        let reg = TEXTURE_REGISTRY.lock().unwrap();
        let w = reg.sprite_sheet_dimensions.0 as f32;
        let h = reg.sprite_sheet_dimensions.1 as f32;

        let info = self.get_info(block_side, &reg);

        let bl = [info.left() as f32 / w, info.top() as f32 / h];
        let br = [info.right() as f32 / w, info.top() as f32 / h];
        let tr = [info.right() as f32 / w, info.bottom() as f32 / h];
        let tl = [info.left() as f32 / w, info.bottom() as f32 / h];

        [tl, tr, br, bl]
    }

    pub fn get_tex_offset(&self, block_side: u8) -> [f32; 2] {
        let reg = TEXTURE_REGISTRY.lock().unwrap();
        let w = reg.sprite_sheet_dimensions.0 as f32;
        let h = reg.sprite_sheet_dimensions.1 as f32;

        let info = self.get_info(block_side, &reg);

        [info.left() as f32 / w, info.top() as f32 / h]
    }
}

#[derive(Clone, Debug)]
pub struct BlockInfo {
    pub module: String,
    pub name: String,
    pub material: BlockMaterial,
    pub texture: BlockTexturing,
    pub opaque: bool,
    pub entity_class: Option<SCM>,
}

lazy_static! {
    pub static ref BLOCK_REGISTRY: Arc<Mutex<BlockRegistry>> =
        Arc::new(Mutex::new(BlockRegistry::default()));
}

#[derive(Debug)]
pub struct BlockRegistry {
    pub info: Vec<BlockInfo>,
    pub map: HashMap<(String, String), BlockId>,
}

impl Default for BlockRegistry {
    fn default() -> Self {
        BlockRegistry::new()
    }
}

impl BlockRegistry {
    pub fn new() -> BlockRegistry {
        BlockRegistry {
            info: Vec::new(),
            map: HashMap::new(),
        }
    }

    pub fn register(&mut self, info: BlockInfo) -> BlockId {
        let id = self.info.len() as u32;
        self.info.push(info.clone());
        self.map.insert((info.module, info.name), id);
        id
    }

    pub fn get(&self, blk: BlockId) -> Option<BlockInfo> {
        self.info.get(blk as usize).cloned()
    }

    pub fn get_id<S: Into<String>>(&self, module: S, name: S) -> Option<BlockId> {
        self.map.get(&(module.into(), name.into())).copied()
    }
}
