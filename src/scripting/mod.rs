use crate::{
    graphics::registry::{TextureRegistry, TEXTURE_REGISTRY},
    world::registry::{BlockInfo, BlockMaterial, BlockTexturing, BLOCK_REGISTRY},
};
use guile_scheme::{
    define, define_subr0, define_subr1, define_subr2, define_subr3, define_subr6, primitive_load,
    scm::goops::load_goops, SCM,
};
use std::sync::MutexGuard;

extern "C" fn scm_textures_all(a: SCM) -> SCM {
    let id: u32 = a.into();
    SCM::from_any(BlockTexturing::All(id))
}

extern "C" fn scm_textures_nothing() -> SCM {
    SCM::from_any(BlockTexturing::None)
}

extern "C" fn scm_textures_pillar(side: SCM, top: SCM, bottom: SCM) -> SCM {
    SCM::from_any(BlockTexturing::Pillar {
        side: side.into(),
        top: top.into(),
        bottom: bottom.into(),
    })
}

extern "C" fn scm_register_texture(module: SCM, name: SCM, path: SCM) -> SCM {
    TEXTURE_REGISTRY
        .lock()
        .unwrap()
        .register(module.into(), name.into(), path.into())
        .into()
}

extern "C" fn scm_texture_id(module: SCM, name: SCM) -> SCM {
    let lock: MutexGuard<TextureRegistry> = TEXTURE_REGISTRY.lock().unwrap();

    let id = lock.get_id(module.into(), name.into());

    id.map(SCM::from).unwrap_or_else(|| SCM::from(-1))
}

extern "C" fn scm_register_block(
    module: SCM,
    name: SCM,
    texture: SCM,
    material: SCM,
    opaque: SCM,
    entity_class: SCM,
) -> SCM {
    let module: String = module.into();
    let name: String = name.into();
    let texture: Box<BlockTexturing> = texture.to_any();

    let material: String = material.into();
    let material = match material.as_str() {
        "stone" => BlockMaterial::Stone,
        "dirt" => BlockMaterial::Dirt,
        "wood" => BlockMaterial::Wood,
        "grass" => BlockMaterial::Grass,
        _ => BlockMaterial::Nothing,
    };

    let opaque: bool = opaque.into();

    let entity_class = if entity_class.is_false() {
        None
    } else {
        Some(entity_class)
    };

    BLOCK_REGISTRY
        .lock()
        .unwrap()
        .register(BlockInfo {
            module,
            name,
            material,
            texture: *texture,
            opaque,
            entity_class,
        })
        .into()
}

fn setup_scripting_api() -> std::io::Result<()> {
    load_goops();

    define_subr2("texture-id", scm_texture_id);
    define_subr1("textures-all", scm_textures_all);
    define_subr3("textures-pillar", scm_textures_pillar);
    define_subr0("textures-nothing", scm_textures_nothing);
    define_subr3("register-texture", scm_register_texture);
    define_subr6("register-block", scm_register_block);

    primitive_load("resources/scripts/macros.scm");
    primitive_load("resources/scripts/block-entity.scm");

    std::fs::read_dir("resources/scripts/modules")?
        .map(|res| res.map(|x| x.path()))
        .filter(|x| x.is_ok())
        .map(|x| x.unwrap())
        .filter(|x| x.is_file())
        .map(|x| x.file_name().unwrap().to_os_string().into_string().unwrap())
        .for_each(|x| {
            primitive_load("resources/scripts/modules/".to_owned() + &x);
            println!("Loaded Script: \"{}\"", x);
        });

    Ok(())
}

/// Defines Api Procedures and loads modules for client
pub fn setup_scripting_api_client() -> std::io::Result<()> {
    setup_scripting_api()?;

    define("on-server", false.into());

    Ok(())
}

/// Defines Api Procedures and loads modules for server
pub fn setup_scripting_api_server() -> std::io::Result<()> {
    setup_scripting_api()?;

    define("on-server", true.into());

    Ok(())
}
