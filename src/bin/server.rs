use guile_scheme::boot_guile;
use sandbox::error::Error;
use std::os::raw::{c_char, c_int, c_void};

fn inner_main() -> Result<(), Error> {
    Ok(())
}

unsafe extern "C" fn inner_main_c(_closure: *mut c_void, _argc: c_int, _argv: *mut *mut c_char) {
    match inner_main() {
        Ok(()) => println!("Exited successfully."),
        Err(e) => println!("Exited with error: {:#?}", e),
    }
}

fn main() {
    boot_guile(inner_main_c);
}
