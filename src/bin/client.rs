use guile_scheme::boot_guile;
use laminar::Socket;
use sandbox::game::context::run_client;
use sandbox::{
    error::Error, graphics::registry::TEXTURE_REGISTRY, scripting::setup_scripting_api_client,
};
use std::os::raw::{c_char, c_int, c_void};

fn inner_main() -> Result<(), Error> {
    env_logger::init();

    lazy_static::initialize(&TEXTURE_REGISTRY);

    setup_scripting_api_client()?;

    TEXTURE_REGISTRY.lock().unwrap().pack();

    let socket = Socket::bind("127.0.0.1:12345")?;

    run_client("sandbox", 1920, 1080, 60.0, socket)
}

unsafe extern "C" fn inner_main_c(_closure: *mut c_void, _argc: c_int, _argv: *mut *mut c_char) {
    match inner_main() {
        Ok(()) => log::info!("Exited successfully."),
        Err(e) => log::error!("Exited with error: {:#?}", e),
    }
}

fn main() {
    boot_guile(inner_main_c);
}
