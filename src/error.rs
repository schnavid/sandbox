use crate::world::WorldError;
use laminar::ErrorKind;
use rendy::graph::GraphBuildError;
use rendy::init::WindowedRendyAutoInitError;

#[derive(Debug)]
pub enum Error {
    //    VulkanError(crate::graphics::vulkan::error::VulkanError),
    WorldError(WorldError),
    IoError(std::io::Error),
    NetworkError(laminar::ErrorKind),
    WindowedRendyAutoInitError(WindowedRendyAutoInitError),
    GraphBuildError(GraphBuildError),
}

pub type GameResult<T> = Result<T, Error>;

impl From<WorldError> for Error {
    fn from(e: WorldError) -> Self {
        Error::WorldError(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::IoError(e)
    }
}

impl From<ErrorKind> for Error {
    fn from(e: ErrorKind) -> Self {
        Error::NetworkError(e)
    }
}

impl From<WindowedRendyAutoInitError> for Error {
    fn from(e: WindowedRendyAutoInitError) -> Self {
        Error::WindowedRendyAutoInitError(e)
    }
}

impl From<GraphBuildError> for Error {
    fn from(e: GraphBuildError) -> Self {
        Error::GraphBuildError(e)
    }
}
