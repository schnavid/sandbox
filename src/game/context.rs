use {
    crate::{
        error::GameResult,
        graphics::{camera::FirstPersonCamera, GraphicsContext},
        world::{Chunk, World},
    },
    laminar::Socket,
    noise::{Perlin, Seedable},
    rendy::{
        command::Families,
        factory::{Config, Factory},
        hal::Backend,
        init::{
            winit::{
                dpi::{LogicalPosition, LogicalSize},
                event::{
                    DeviceEvent, ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent,
                },
                event_loop::{ControlFlow, EventLoop},
                platform::desktop::EventLoopExtDesktop,
                window::{Window, WindowBuilder},
            },
            AnyWindowedRendy,
        },
        wsi::Surface,
    },
    serde::export::PhantomData,
    std::{
        collections::{HashMap, VecDeque},
        sync::Mutex,
        time::{Duration, Instant},
    },
};

pub struct GameContext<B: Backend> {
    pub(crate) world: World,
    pub(crate) camera: FirstPersonCamera,
    pub(crate) chunks_to_render: Vec<(i32, i32, i32)>,
    pub(crate) chunks_to_regenerate: Mutex<VecDeque<(i32, i32, i32)>>,
    phantom: PhantomData<B>,
    pub(crate) dimensions: (u32, u32),
}

fn run<B: Backend>(
    mut event_loop: EventLoop<()>,
    factory: Factory<B>,
    families: Families<B>,
    surface: Surface<B>,
    window: Window,
    fps: f32,
    _socket: Socket,
) -> GameResult<()> {
    let (mut graphics, graph_builder) = GraphicsContext::new(factory, families, surface, window)?;
    let mut world = World::new();

    let perlin = Perlin::new();
    perlin.set_seed(0);

    world.set(0, 0, 0, Chunk::generate(&perlin, 0, 0, 0));
    world.set(1, 0, 0, Chunk::generate(&perlin, 1, 0, 0));
    world.set(1, 0, 1, Chunk::generate(&perlin, 1, 0, 1));
    world.set(0, 0, 1, Chunk::generate(&perlin, 0, 0, 1));

    let mut ctx = GameContext {
        world,
        camera: FirstPersonCamera::new(),
        chunks_to_render: vec![(0, 0, 0), (1, 0, 0), (1, 0, 1), (0, 0, 1)],
        chunks_to_regenerate: Mutex::new(
            vec![(0, 0, 0), (1, 0, 0), (1, 0, 1), (0, 0, 1)]
                .into_iter()
                .collect(),
        ),
        phantom: PhantomData,
        dimensions: graphics.window.inner_size().into(),
    };

    graphics.init(&ctx, graph_builder)?;

    let mut clock = Instant::now();
    let mut accumulator = Duration::from_micros(0);
    let update_time = Duration::from_secs_f32(1.0 / fps);

    let mut pressed_keys = HashMap::new();

    let mut done = false;
    while !done {
        event_loop.run_return(|event, _, control_flow| match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::Resized(_) => {}
                WindowEvent::Moved(_) => {}
                WindowEvent::CloseRequested => {
                    *control_flow = ControlFlow::Exit;
                    done = true;
                }
                WindowEvent::Destroyed => {}
                WindowEvent::DroppedFile(_) => {}
                WindowEvent::HoveredFile(_) => {}
                WindowEvent::HoveredFileCancelled => {}
                WindowEvent::ReceivedCharacter(_) => {}
                WindowEvent::Focused(_) => {}
                WindowEvent::KeyboardInput { .. } => {}
                WindowEvent::CursorMoved { .. } => {}
                WindowEvent::CursorEntered { .. } => {}
                WindowEvent::CursorLeft { .. } => {}
                WindowEvent::MouseWheel { .. } => {}
                WindowEvent::MouseInput { .. } => {}
                WindowEvent::TouchpadPressure { .. } => {}
                WindowEvent::AxisMotion { .. } => {}
                WindowEvent::Touch(_) => {}
                WindowEvent::ScaleFactorChanged { .. } => {}
                WindowEvent::ThemeChanged(_) => {}
            },
            Event::DeviceEvent {
                event: DeviceEvent::Key(i),
                ..
            } => match i {
                KeyboardInput {
                    virtual_keycode: Some(kc),
                    state: ElementState::Pressed,
                    ..
                } => {
                    pressed_keys.insert(kc, true);
                }
                KeyboardInput {
                    virtual_keycode: Some(kc),
                    state: ElementState::Released,
                    ..
                } => {
                    pressed_keys.insert(kc, false);
                }
                _ => {}
            },
            Event::DeviceEvent {
                event: DeviceEvent::MouseMotion { delta: d },
                ..
            } => {
                ctx.camera.handle_mouse(d.0 as f32, -d.1 as f32);
            }
            Event::DeviceEvent { .. } => {}
            Event::UserEvent(_) => {}
            Event::Suspended => {}
            Event::Resumed => {}
            Event::MainEventsCleared => {
                *control_flow = ControlFlow::Exit;
            }
            Event::RedrawRequested(_) => {}
            Event::RedrawEventsCleared => {}
            Event::LoopDestroyed => {}
            Event::NewEvents(_) => {}
        });

        while accumulator >= update_time {
            accumulator -= update_time;

            for (&kc, &p) in pressed_keys.iter() {
                if p {
                    match kc {
                        VirtualKeyCode::W => ctx.camera.move_forward(0.05),
                        VirtualKeyCode::S => ctx.camera.move_forward(-0.05),
                        VirtualKeyCode::D => ctx.camera.move_sideways(0.05),
                        VirtualKeyCode::A => ctx.camera.move_sideways(-0.05),
                        VirtualKeyCode::Space => ctx.camera.move_upwards(0.05),
                        VirtualKeyCode::LShift => ctx.camera.move_upwards(-0.05),
                        VirtualKeyCode::Escape => {
                            done = true;
                        }
                        _ => {}
                    }
                }
            }

            ctx.world.update();
        }

        graphics.render(&mut ctx);

        let size = graphics
            .window
            .inner_size()
            .to_logical::<f32>(graphics.window.scale_factor());
        graphics
            .window
            .set_cursor_position(LogicalPosition::new(size.width * 0.5, size.height * 0.5))
            .unwrap();

        accumulator += clock.elapsed();
        clock = Instant::now();
    }

    graphics.dispose(&ctx);

    Ok(())
}

pub fn run_client<S: Into<String>>(
    title: S,
    width: u32,
    height: u32,
    fps: f32,
    socket: Socket,
) -> GameResult<()> {
    let config: Config = Default::default();

    let event_loop = EventLoop::new();
    let window_builder = WindowBuilder::new()
        .with_inner_size(LogicalSize::new(width as u32, height as u32))
        .with_title(title);

    let mut error = None;

    let rendy = AnyWindowedRendy::init_auto(&config, window_builder, &event_loop)?;
    rendy::with_any_windowed_rendy!((rendy)
    (factory, families, surface, window) => {
        match run(event_loop, factory, families, surface, window, fps, socket) {
            Ok(()) => {},
            Err(e) => error = Some(e)
        }
    });

    match error {
        Some(err) => Err(err),
        None => Ok(()),
    }
}
