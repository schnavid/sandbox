//#![warn(missing_docs)]

//! A little sandbox game

#[macro_use]
extern crate lazy_static;

/// Errors
pub mod error;
pub mod game;
/// Rendering, Texture Registry, etc.
pub mod graphics;
/// Connection between server and client
pub mod network;
/// Interface to the scripting language
pub mod scripting;
/// World Format, World Generation, etc.
pub mod world;

/// Default address to start the server on
pub const SERVER_ADDR: &str = "127.0.0.1:80080";
