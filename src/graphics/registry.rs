use image::{GenericImageView, ImageFormat};
use sheep::{Format, InputSprite, MaxrectsOptions, MaxrectsPacker, SpriteAnchor};
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::sync::Arc;
use std::sync::Mutex;

pub type TextureId = u32;

#[derive(Debug, Clone)]
pub struct TextureInfo {
    pub module: String,
    pub name: String,
    anchor: sheep::SpriteAnchor,
}

impl TextureInfo {
    pub fn left(&self) -> u32 {
        self.anchor.position.0
    }

    pub fn right(&self) -> u32 {
        self.anchor.position.0 + self.anchor.dimensions.0
    }

    pub fn top(&self) -> u32 {
        self.anchor.position.1
    }

    pub fn bottom(&self) -> u32 {
        self.anchor.position.1 + self.anchor.dimensions.1
    }
}

lazy_static! {
    pub static ref TEXTURE_REGISTRY: Arc<Mutex<TextureRegistry>> =
        Arc::new(Mutex::new(TextureRegistry::default()));
}

pub type TextureNameIdMap = HashMap<(String, String), TextureId>;

struct SpriteSheetFormat;

impl Format for SpriteSheetFormat {
    type Data = ((u32, u32), Vec<TextureInfo>, TextureNameIdMap);
    type Options = Vec<(String, String)>;

    fn encode(
        dimensions: (u32, u32),
        sprites: &[SpriteAnchor],
        options: Self::Options,
    ) -> Self::Data {
        (
            dimensions,
            sprites
                .iter()
                .map(|s| TextureInfo {
                    module: options[s.id].0.clone(),
                    name: options[s.id].1.clone(),
                    anchor: *s,
                })
                .collect(),
            sprites
                .iter()
                .map(|s| (options[s.id].clone(), s.id as u32))
                .collect(),
        )
    }
}

pub struct TextureRegistry {
    pub info: Vec<TextureInfo>,
    register_info: Vec<(String, String, String)>,
    pub map: TextureNameIdMap,
    pub sprite_sheet: Vec<u8>,
    pub sprite_sheet_dimensions: (u32, u32),
}

impl Default for TextureRegistry {
    fn default() -> Self {
        TextureRegistry::new()
    }
}

impl TextureRegistry {
    pub fn new() -> TextureRegistry {
        TextureRegistry {
            info: Vec::new(),
            register_info: Vec::new(),
            map: HashMap::new(),
            sprite_sheet: Vec::new(),
            sprite_sheet_dimensions: (0, 0),
        }
    }

    pub fn register(&mut self, module: String, name: String, path: String) -> TextureId {
        let id = self.register_info.len() as u32;
        self.register_info.push((module, name, path));
        id
    }

    pub fn pack(&mut self) {
        let result = sheep::pack::<MaxrectsPacker>(
            self.register_info
                .iter()
                .map(|(m, n, p)| {
                    let mut bytes = Vec::new();
                    File::open("resources/textures/".to_owned() + p)
                        .unwrap()
                        .read_to_end(&mut bytes)
                        .unwrap();

                    let img =
                        image::load_from_memory_with_format(bytes.as_slice(), ImageFormat::PNG)
                            .unwrap();
                    let bytes = img.to_rgba().into_raw();

                    println!(
                        "Packing Sprite: {}:{}, dimensions: {:?}",
                        m,
                        n,
                        img.dimensions()
                    );

                    InputSprite {
                        bytes,
                        dimensions: img.dimensions(),
                    }
                })
                .collect(),
            4,
            MaxrectsOptions::default(),
        );

        let (dimensions, info, map) = sheep::encode::<SpriteSheetFormat>(
            &result[0],
            self.register_info
                .iter()
                .cloned()
                .map(|(m, n, _)| (m, n))
                .collect(),
        );

        println!(
            "Resulting Sprite sheet: size: {} bytes, dimensions: {:?}",
            result[0].bytes.len(),
            result[0].dimensions
        );

        self.sprite_sheet_dimensions = dimensions;
        self.info = info;
        self.map = map;
        self.sprite_sheet = result[0].clone().bytes;
    }

    pub fn get(&self, blk: TextureId) -> Option<TextureInfo> {
        self.info.get(blk as usize).cloned()
    }

    pub fn get_id(&self, module: String, name: String) -> Option<TextureId> {
        self.map.get(&(module, name)).copied()
    }
}
