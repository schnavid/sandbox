use crate::{
    graphics::vertex::BlockVertex as Vertex,
    world::{
        registry::{BlockInfo, BlockMaterial, BlockRegistry, BLOCK_REGISTRY},
        World, WorldError,
    },
};

struct BlockSurroundingsOpen {
    front: bool,
    right: bool,
    back: bool,
    left: bool,
    top: bool,
    bottom: bool,
    front_top: bool,
    front_top_right: bool,
    front_right: bool,
    front_bottom_right: bool,
    front_bottom: bool,
    front_bottom_left: bool,
    front_left: bool,
    front_top_left: bool,
    right_top: bool,
    right_bottom: bool,
    left_top: bool,
    left_bottom: bool,
    back_top: bool,
    back_top_right: bool,
    back_right: bool,
    back_bottom_right: bool,
    back_bottom: bool,
    back_bottom_left: bool,
    back_left: bool,
    back_top_left: bool,
}

impl BlockSurroundingsOpen {
    fn new(
        world: &World,
        chunk_pos: (i32, i32, i32),
        (x, y, z): (i8, i8, i8),
        registry: &BlockRegistry,
    ) -> BlockSurroundingsOpen {
        fn get_block_not_opaque(
            (cx, cy, cz): (i32, i32, i32),
            (x, y, z): (i8, i8, i8),
            registry: &BlockRegistry,
            world: &World,
        ) -> bool {
            let (cx, x) = if x == 16 {
                (cx + 1, 0)
            } else if x == -1 {
                (cx - 1, 15)
            } else {
                (cx, x)
            };
            let (cy, y) = if y == 16 || y == -1 {
                return true;
            } else {
                (cy, y)
            };
            let (cz, z) = if z == 16 {
                (cz + 1, 0)
            } else if z == -1 {
                (cz - 1, 15)
            } else {
                (cz, z)
            };

            world
                .get(cx, cy, cz)
                .map(|c| {
                    registry
                        .get(c[(x as usize, y as usize, z as usize)])
                        .map(|info| !info.opaque)
                        .unwrap_or(true)
                })
                .unwrap_or(true)
        }

        BlockSurroundingsOpen {
            front: get_block_not_opaque(chunk_pos, (x, y, z + 1), &registry, world),
            right: get_block_not_opaque(chunk_pos, (x + 1, y, z), &registry, world),
            back: get_block_not_opaque(chunk_pos, (x, y, z - 1), &registry, world),
            left: get_block_not_opaque(chunk_pos, (x - 1, y, z), &registry, world),
            top: get_block_not_opaque(chunk_pos, (x, y + 1, z), &registry, world),
            bottom: get_block_not_opaque(chunk_pos, (x, y - 1, z), &registry, world),
            front_top: get_block_not_opaque(chunk_pos, (x, y + 1, z + 1), &registry, world),
            front_top_right: get_block_not_opaque(
                chunk_pos,
                (x + 1, y + 1, z + 1),
                &registry,
                world,
            ),
            front_right: get_block_not_opaque(chunk_pos, (x + 1, y, z + 1), &registry, world),
            front_bottom_right: get_block_not_opaque(
                chunk_pos,
                (x + 1, y - 1, z + 1),
                &registry,
                world,
            ),
            front_bottom: get_block_not_opaque(chunk_pos, (x, y - 1, z + 1), &registry, world),
            front_bottom_left: get_block_not_opaque(
                chunk_pos,
                (x - 1, y - 1, z + 1),
                &registry,
                world,
            ),
            front_left: get_block_not_opaque(chunk_pos, (x - 1, y, z + 1), &registry, world),
            front_top_left: get_block_not_opaque(
                chunk_pos,
                (x - 1, y + 1, z + 1),
                &registry,
                world,
            ),
            right_top: get_block_not_opaque(chunk_pos, (x + 1, y + 1, z), &registry, world),
            right_bottom: get_block_not_opaque(chunk_pos, (x + 1, y - 1, z), &registry, world),
            left_top: get_block_not_opaque(chunk_pos, (x - 1, y + 1, z), &registry, world),
            left_bottom: get_block_not_opaque(chunk_pos, (x - 1, y - 1, z), &registry, world),
            back_top: get_block_not_opaque(chunk_pos, (x, y + 1, z - 1), &registry, world),
            back_top_right: get_block_not_opaque(
                chunk_pos,
                (x + 1, y + 1, z - 1),
                &registry,
                world,
            ),
            back_right: get_block_not_opaque(chunk_pos, (x + 1, y, z - 1), &registry, world),
            back_bottom_right: get_block_not_opaque(
                chunk_pos,
                (x + 1, y - 1, z - 1),
                &registry,
                world,
            ),
            back_bottom: get_block_not_opaque(chunk_pos, (x, y - 1, z - 1), &registry, world),
            back_bottom_left: get_block_not_opaque(
                chunk_pos,
                (x - 1, y - 1, z - 1),
                &registry,
                world,
            ),
            back_left: get_block_not_opaque(chunk_pos, (x - 1, y, z - 1), &registry, world),
            back_top_left: get_block_not_opaque(chunk_pos, (x - 1, y + 1, z - 1), &registry, world),
        }
    }
}

pub fn generate_vertices(
    world: &World,
    cx: i32,
    cy: i32,
    cz: i32,
) -> Result<(Vec<Vertex>, Vec<u32>), WorldError> {
    let registry = BLOCK_REGISTRY.lock().unwrap();

    let mut vertices = Vec::new();
    let mut indices = Vec::new();
    let chunk = world.get(cx, cy, cz).ok_or(WorldError::ChunkNotLoaded)?;

    for x in 0..16 {
        for y in 0..16 {
            for z in 0..16 {
                let blk = chunk[(x, y, z)];
                let info = registry
                    .get(blk)
                    .ok_or_else(|| WorldError::BlockNotRegistered(blk))?;

                if info.material != BlockMaterial::Nothing {
                    let (bv, bi) = block_vertices(
                        BlockSurroundingsOpen::new(
                            world,
                            (cx, cy, cz),
                            (x as i8, y as i8, z as i8),
                            &registry,
                        ),
                        [
                            (cx * 16 + x as i32) as f32,
                            (cy * 16 + y as i32) as f32,
                            (cz * 16 + z as i32) as f32,
                        ],
                        info,
                    );

                    indices.extend(bi.iter().map(|i| i + vertices.len() as u32));
                    vertices.extend(bv.iter().cloned());
                }
            }
        }
    }

    Ok((vertices, indices))
}

fn block_vertices(
    sides: BlockSurroundingsOpen,
    pos: [f32; 3],
    info: BlockInfo,
) -> (Vec<Vertex>, Vec<u32>) {
    let (x, y, z) = (pos[0], pos[1], pos[2]);

    let mut vertices = Vec::new();
    let mut indices = Vec::new();

    fn add_side(len: usize, indices: &mut Vec<u32>) {
        let len = len as u32;
        indices.extend_from_slice(&[len, len + 1, len + 2, len + 2, len + 3, len]);
    }

    fn vertex_ao(side1: bool, side2: bool, corner: bool) -> u32 {
        if !side1 && !side2 {
            0
        } else {
            3 - (!side1 as u32 + !side2 as u32 + !corner as u32)
        }
    }

    if sides.front {
        add_side(vertices.len(), &mut indices);

        let tex = info.texture.get_tex_coords(0);
        vertices.extend_from_slice(&[
            Vertex::new(
                [x + 0.0, y + 0.0, z + 1.0],
                [0.0, 0.0, 1.0],
                tex[0],
                [0.0, 0.0],
                vertex_ao(
                    sides.front_left,
                    sides.front_bottom,
                    sides.front_bottom_left,
                ),
            ),
            Vertex::new(
                [x + 1.0, y + 0.0, z + 1.0],
                [0.0, 0.0, 1.0],
                tex[1],
                [1.0, 0.0],
                vertex_ao(
                    sides.front_right,
                    sides.front_bottom,
                    sides.front_bottom_right,
                ),
            ),
            Vertex::new(
                [x + 1.0, y + 1.0, z + 1.0],
                [0.0, 0.0, 1.0],
                tex[2],
                [1.0, 1.0],
                vertex_ao(sides.front_right, sides.front_top, sides.front_top_right),
            ),
            Vertex::new(
                [x + 0.0, y + 1.0, z + 1.0],
                [0.0, 0.0, 1.0],
                tex[3],
                [0.0, 1.0],
                vertex_ao(sides.front_left, sides.front_top, sides.front_top_left),
            ),
        ]);
    }

    if sides.right {
        add_side(vertices.len(), &mut indices);

        let tex = info.texture.get_tex_coords(1);
        vertices.extend_from_slice(&[
            Vertex::new(
                [x + 1.0, y + 0.0, z + 1.0],
                [1.0, 0.0, 0.0],
                tex[0],
                [0.0, 0.0],
                vertex_ao(
                    sides.front_right,
                    sides.right_bottom,
                    sides.front_bottom_right,
                ),
            ),
            Vertex::new(
                [x + 1.0, y + 0.0, z + 0.0],
                [1.0, 0.0, 0.0],
                tex[1],
                [1.0, 0.0],
                vertex_ao(
                    sides.back_right,
                    sides.right_bottom,
                    sides.back_bottom_right,
                ),
            ),
            Vertex::new(
                [x + 1.0, y + 1.0, z + 0.0],
                [1.0, 0.0, 0.0],
                tex[2],
                [1.0, 1.0],
                vertex_ao(sides.back_right, sides.right_top, sides.back_top_right),
            ),
            Vertex::new(
                [x + 1.0, y + 1.0, z + 1.0],
                [1.0, 0.0, 0.0],
                tex[3],
                [0.0, 1.0],
                vertex_ao(sides.front_right, sides.right_top, sides.front_top_right),
            ),
        ])
    }

    if sides.back {
        add_side(vertices.len(), &mut indices);

        let tex = info.texture.get_tex_coords(2);
        vertices.extend_from_slice(&[
            Vertex::new(
                [x + 1.0, y + 0.0, z + 0.0],
                [0.0, 0.0, -1.0],
                tex[0],
                [0.0, 0.0],
                vertex_ao(sides.back_right, sides.back_bottom, sides.back_bottom_right),
            ),
            Vertex::new(
                [x + 0.0, y + 0.0, z + 0.0],
                [0.0, 0.0, -1.0],
                tex[1],
                [1.0, 0.0],
                vertex_ao(sides.back_left, sides.back_bottom, sides.back_bottom_left),
            ),
            Vertex::new(
                [x + 0.0, y + 1.0, z + 0.0],
                [0.0, 0.0, -1.0],
                tex[2],
                [1.0, 1.0],
                vertex_ao(sides.back_left, sides.back_top, sides.back_top_left),
            ),
            Vertex::new(
                [x + 1.0, y + 1.0, z + 0.0],
                [0.0, 0.0, -1.0],
                tex[3],
                [0.0, 1.0],
                vertex_ao(sides.back_right, sides.back_top, sides.back_top_right),
            ),
        ])
    }

    if sides.left {
        add_side(vertices.len(), &mut indices);

        let tex = info.texture.get_tex_coords(3);
        vertices.extend_from_slice(&[
            Vertex::new(
                [x + 0.0, y + 0.0, z + 0.0],
                [-1.0, 0.0, 0.0],
                tex[0],
                [0.0, 0.0],
                vertex_ao(sides.back_left, sides.left_bottom, sides.back_bottom_left),
            ),
            Vertex::new(
                [x + 0.0, y + 0.0, z + 1.0],
                [-1.0, 0.0, 0.0],
                tex[1],
                [1.0, 0.0],
                vertex_ao(sides.front_left, sides.left_bottom, sides.front_bottom_left),
            ),
            Vertex::new(
                [x + 0.0, y + 1.0, z + 1.0],
                [-1.0, 0.0, 0.0],
                tex[2],
                [1.0, 1.0],
                vertex_ao(sides.front_left, sides.left_top, sides.front_top_left),
            ),
            Vertex::new(
                [x + 0.0, y + 1.0, z + 0.0],
                [-1.0, 0.0, 0.0],
                tex[3],
                [0.0, 1.0],
                vertex_ao(sides.back_left, sides.left_top, sides.back_top_left),
            ),
        ])
    }

    if sides.top {
        add_side(vertices.len(), &mut indices);

        let tex = info.texture.get_tex_coords(4);
        vertices.extend_from_slice(&[
            Vertex::new(
                [x + 0.0, y + 1.0, z + 1.0],
                [0.0, 1.0, 0.0],
                tex[0],
                [0.0, 0.0],
                vertex_ao(sides.left_top, sides.front_top, sides.front_top_left),
            ),
            Vertex::new(
                [x + 1.0, y + 1.0, z + 1.0],
                [0.0, 1.0, 0.0],
                tex[1],
                [1.0, 0.0],
                vertex_ao(sides.right_top, sides.front_top, sides.front_top_right),
            ),
            Vertex::new(
                [x + 1.0, y + 1.0, z + 0.0],
                [0.0, 1.0, 0.0],
                tex[2],
                [1.0, 1.0],
                vertex_ao(sides.right_top, sides.back_top, sides.back_top_right),
            ),
            Vertex::new(
                [x + 0.0, y + 1.0, z + 0.0],
                [0.0, 1.0, 0.0],
                tex[3],
                [0.0, 1.0],
                vertex_ao(sides.left_top, sides.back_top, sides.back_top_left),
            ),
        ])
    }

    if sides.bottom {
        add_side(vertices.len(), &mut indices);

        let tex = info.texture.get_tex_coords(5);
        vertices.extend_from_slice(&[
            Vertex::new(
                [x + 1.0, y + 0.0, z + 1.0],
                [0.0, -1.0, 0.0],
                tex[0],
                [0.0, 0.0],
                vertex_ao(
                    sides.right_bottom,
                    sides.front_bottom,
                    sides.front_bottom_right,
                ),
            ),
            Vertex::new(
                [x + 0.0, y + 0.0, z + 1.0],
                [0.0, -1.0, 0.0],
                tex[1],
                [1.0, 0.0],
                vertex_ao(
                    sides.left_bottom,
                    sides.front_bottom,
                    sides.front_bottom_left,
                ),
            ),
            Vertex::new(
                [x + 0.0, y + 0.0, z + 0.0],
                [0.0, -1.0, 0.0],
                tex[2],
                [1.0, 1.0],
                vertex_ao(sides.left_bottom, sides.back_bottom, sides.back_bottom_left),
            ),
            Vertex::new(
                [x + 1.0, y + 0.0, z + 0.0],
                [0.0, -1.0, 0.0],
                tex[3],
                [0.0, 1.0],
                vertex_ao(
                    sides.right_bottom,
                    sides.back_bottom,
                    sides.back_bottom_right,
                ),
            ),
        ])
    }

    (vertices, indices)
}
