use crate::{
    error::GameResult, game::context::GameContext,
    graphics::pipelines::chunks::ChunksGraphicsPipeline,
};
use rendy::{
    command::Families,
    factory::Factory,
    graph::{
        present::PresentNode,
        render::{RenderGroupBuilder, SimpleGraphicsPipeline},
        Graph, GraphBuilder,
    },
    hal::{self, Backend},
    init::winit::dpi::PhysicalSize,
    init::winit::window::Window,
    wsi::Surface,
};

pub mod camera;
pub mod chunk_rendering;
pub mod pipelines;
pub mod registry;
pub mod vertex;

pub struct GraphicsContext<B: Backend> {
    pub(crate) graph: Option<Graph<B, GameContext<B>>>,
    pub(crate) factory: Factory<B>,
    pub(crate) families: Families<B>,
    pub(crate) window: Window,
}

pub type GraphicsContextNewResult<B> =
    GameResult<(GraphicsContext<B>, GraphBuilder<B, GameContext<B>>)>;

impl<B: Backend> GraphicsContext<B> {
    pub fn new(
        factory: Factory<B>,
        families: Families<B>,
        surface: Surface<B>,
        window: Window,
    ) -> GraphicsContextNewResult<B> {
        let mut graph_builder = GraphBuilder::new();

        let _size = window.inner_size();
        let size = PhysicalSize::new(1920, 1080); // TODO: Handle Window resizing

        let color = graph_builder.create_image(
            hal::image::Kind::D2(size.width, size.height, 1, 1),
            1,
            factory.get_surface_format(&surface),
            Some(hal::command::ClearValue {
                color: hal::command::ClearColor {
                    float32: [0.01, 0.01, 0.02, 1.0],
                },
            }),
        );

        let depth = graph_builder.create_image(
            hal::image::Kind::D2(size.width, size.height, 1, 1),
            1,
            hal::format::Format::D32Sfloat,
            Some(hal::command::ClearValue {
                depth_stencil: hal::command::ClearDepthStencil {
                    depth: 1.0,
                    stencil: 0,
                },
            }),
        );

        let pass = graph_builder.add_node(
            ChunksGraphicsPipeline::builder()
                .into_subpass()
                .with_color(color)
                .with_depth_stencil(depth)
                .into_pass(),
        );

        graph_builder
            .add_node(PresentNode::builder(&factory, surface, color).with_dependency(pass));

        Ok((
            GraphicsContext {
                graph: None,
                families,
                factory,
                window,
            },
            graph_builder,
        ))
    }

    pub fn init(
        &mut self,
        ctx: &GameContext<B>,
        graph_builder: GraphBuilder<B, GameContext<B>>,
    ) -> GameResult<()> {
        self.graph = Some(graph_builder.build(&mut self.factory, &mut self.families, &ctx)?);

        Ok(())
    }

    pub fn render(&mut self, ctx: &mut GameContext<B>) {
        ctx.dimensions = self.window.inner_size().into();

        self.factory.maintain(&mut self.families);
        if let Some(ref mut graph) = self.graph {
            graph.run(&mut self.factory, &mut self.families, ctx);
        }
    }

    pub fn dispose(&mut self, ctx: &GameContext<B>) {
        if let Some(graph) = self.graph.take() {
            graph.dispose(&mut self.factory, ctx)
        }
    }
}
