use rendy::{
    command::{QueueId, RenderPassEncoder},
    factory::{Factory, ImageState},
    graph::{render::*, GraphContext, NodeBuffer, NodeImage},
    hal::{self, adapter::PhysicalDevice, device::Device, Backend},
    memory::Dynamic,
    resource::{
        Buffer, BufferInfo, DescriptorSet, DescriptorSetLayout, Escape, Handle, SamplerDesc,
    },
    shader::{
        PathBufShaderInfo, ShaderKind, ShaderSetBuilder, SourceLanguage, SpirvReflection,
        SpirvShader,
    },
    texture::{Texture, TextureBuilder},
};

use crate::{
    game::context::GameContext,
    graphics::{camera::Camera, chunk_rendering::generate_vertices, registry::TEXTURE_REGISTRY},
};
use nalgebra::{Matrix4, Perspective3};
use std::collections::HashMap;

lazy_static::lazy_static! {
    static ref VERTEX: SpirvShader = PathBufShaderInfo::new(
        concat!(env!("CARGO_MANIFEST_DIR"), "/resources/shaders/chunks.vert").into(),
        ShaderKind::Vertex,
        SourceLanguage::GLSL,
        "main"
    ).precompile().unwrap();

    static ref FRAGMENT: SpirvShader = PathBufShaderInfo::new(
        concat!(env!("CARGO_MANIFEST_DIR"), "/resources/shaders/chunks.frag").into(),
        ShaderKind::Fragment,
        SourceLanguage::GLSL,
        "main",
    ).precompile().unwrap();

    static ref SHADERS: ShaderSetBuilder = ShaderSetBuilder::default()
        .with_vertex(&*VERTEX).unwrap()
        .with_fragment(&*FRAGMENT).unwrap();

    pub static ref SHADER_REFLECTION: SpirvReflection = SHADERS.reflect().unwrap();
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct UniformArgs {
    world: Matrix4<f32>,
    view: Matrix4<f32>,
    proj: Matrix4<f32>,
}

const UNIFORM_SIZE: u64 = std::mem::size_of::<UniformArgs>() as u64;

#[inline]
fn uniform_buffer_frame_size(align: u64) -> u64 {
    ((UNIFORM_SIZE - 1) / align + 1) * align
}

#[inline]
fn uniform_offset(align: u64, index: u64) -> u64 {
    uniform_buffer_frame_size(align) * index
}

#[derive(Debug, Default)]
pub struct ChunksGraphicsPipelineDesc;

#[derive(Debug)]
struct ChunkData<B: Backend> {
    vbuf: Escape<Buffer<B>>,
    vnum: u64,
    ibuf: Escape<Buffer<B>>,
    inum: u64,
}

impl<B: Backend> ChunkData<B> {
    fn new((x, y, z): (i32, i32, i32), factory: &Factory<B>, ctx: &GameContext<B>) -> ChunkData<B> {
        let (vertices, indices) = generate_vertices(&ctx.world, x, y, z).unwrap();

        let vnum = vertices.len() as u64;
        let inum = indices.len() as u64;

        let vbuf_size = SHADER_REFLECTION.attributes_range(..).unwrap().stride as u64 * vnum;
        let ibuf_size = std::mem::size_of::<u32>() as u64 * inum;

        let mut vbuf = factory
            .create_buffer(
                BufferInfo {
                    size: vbuf_size,
                    usage: hal::buffer::Usage::VERTEX,
                },
                Dynamic,
            )
            .unwrap();

        let mut ibuf = factory
            .create_buffer(
                BufferInfo {
                    size: ibuf_size,
                    usage: hal::buffer::Usage::INDEX,
                },
                Dynamic,
            )
            .unwrap();

        unsafe {
            factory
                .upload_visible_buffer(&mut vbuf, 0, vertices.as_slice())
                .unwrap();
            factory
                .upload_visible_buffer(&mut ibuf, 0, indices.as_slice())
                .unwrap();
        }

        ChunkData {
            vbuf,
            vnum,
            ibuf,
            inum,
        }
    }
}

#[derive(Debug)]
pub struct ChunksGraphicsPipeline<B: Backend> {
    chunks: HashMap<(i32, i32, i32), ChunkData<B>>,
    uniform_buffer: Escape<Buffer<B>>,
    texture: Texture<B>,
    descriptor_set: Escape<DescriptorSet<B>>,
    sets: Vec<Escape<DescriptorSet<B>>>,
    align: u64,
}

impl<B: Backend> SimpleGraphicsPipelineDesc<B, GameContext<B>> for ChunksGraphicsPipelineDesc {
    type Pipeline = ChunksGraphicsPipeline<B>;

    fn depth_stencil(&self) -> Option<hal::pso::DepthStencilDesc> {
        let depth = Some(hal::pso::DepthStencilDesc {
            depth: Some(hal::pso::DepthTest {
                fun: hal::pso::Comparison::Less,
                write: true,
            }),
            depth_bounds: false,
            stencil: Some(Default::default()),
        });

        log::debug!("Chunks Depth Stencil: {:?}", depth);

        depth
    }

    fn rasterizer(&self) -> hal::pso::Rasterizer {
        let rasterizer = hal::pso::Rasterizer {
            polygon_mode: hal::pso::PolygonMode::Fill,
            cull_face: hal::pso::Face::BACK,
            front_face: hal::pso::FrontFace::CounterClockwise,
            depth_clamping: false,
            depth_bias: None,
            conservative: false,
        };

        log::debug!("Chunks Rasterizer: {:?}", rasterizer);

        rasterizer
    }

    fn vertices(
        &self,
    ) -> Vec<(
        Vec<hal::pso::Element<hal::format::Format>>,
        hal::pso::ElemStride,
        hal::pso::VertexInputRate,
    )> {
        let vertices = vec![SHADER_REFLECTION
            .attributes_range(..)
            .unwrap()
            .gfx_vertex_input_desc(hal::pso::VertexInputRate::Vertex)];

        log::debug!("Chunks Vertices: {:?}", vertices);

        vertices
    }

    fn layout(&self) -> Layout {
        let layout = Layout {
            sets: vec![
                SetLayout {
                    bindings: vec![hal::pso::DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: hal::pso::DescriptorType::UniformBuffer,
                        count: 1,
                        stage_flags: hal::pso::ShaderStageFlags::GRAPHICS,
                        immutable_samplers: false,
                    }],
                },
                SetLayout {
                    bindings: vec![
                        hal::pso::DescriptorSetLayoutBinding {
                            binding: 1,
                            ty: hal::pso::DescriptorType::SampledImage,
                            count: 1,
                            stage_flags: hal::pso::ShaderStageFlags::FRAGMENT,
                            immutable_samplers: false,
                        },
                        hal::pso::DescriptorSetLayoutBinding {
                            binding: 2,
                            ty: hal::pso::DescriptorType::Sampler,
                            count: 1,
                            stage_flags: hal::pso::ShaderStageFlags::FRAGMENT,
                            immutable_samplers: false,
                        },
                    ],
                },
            ],
            push_constants: vec![],
        };

        log::debug!("Chunks Shader layout: {:?}", layout);

        layout
    }

    fn load_shader_set(
        &self,
        factory: &mut Factory<B>,
        _ctx: &GameContext<B>,
    ) -> rendy::shader::ShaderSet<B> {
        SHADERS.build(factory, Default::default()).unwrap()
    }

    fn build<'a>(
        self,
        ctx: &GraphContext<B>,
        factory: &mut Factory<B>,
        queue: QueueId,
        _aux: &GameContext<B>,
        buffers: Vec<NodeBuffer>,
        images: Vec<NodeImage>,
        set_layouts: &[Handle<DescriptorSetLayout<B>>],
    ) -> Result<Self::Pipeline, rendy::hal::pso::CreationError> {
        log::trace!("Chunks Started build");

        assert!(buffers.is_empty());
        assert!(images.is_empty());
        assert_eq!(set_layouts.len(), 2);

        let frames = ctx.frames_in_flight as _;
        let align = factory
            .physical()
            .limits()
            .min_uniform_buffer_offset_alignment;

        let uniform_buffer = factory
            .create_buffer(
                BufferInfo {
                    size: uniform_buffer_frame_size(align) * frames,
                    usage: hal::buffer::Usage::UNIFORM,
                },
                Dynamic,
            )
            .unwrap();

        let mut sets = Vec::with_capacity(frames as usize);
        for index in 0..frames {
            unsafe {
                let set = factory
                    .create_descriptor_set(set_layouts[0].clone())
                    .unwrap();
                factory.write_descriptor_sets(Some(hal::pso::DescriptorSetWrite {
                    set: set.raw(),
                    binding: 0,
                    array_offset: 0,
                    descriptors: Some(hal::pso::Descriptor::Buffer(
                        uniform_buffer.raw(),
                        Some(uniform_offset(align, index))..Some(uniform_offset(align, index)),
                    )),
                }));
                sets.push(set);
            }
        }

        let texture = {
            let reg = TEXTURE_REGISTRY.lock().unwrap();

            TextureBuilder::new()
                .with_kind(hal::image::Kind::D2(
                    reg.sprite_sheet_dimensions.0,
                    reg.sprite_sheet_dimensions.1,
                    1,
                    1,
                ))
                .with_raw_data(&reg.sprite_sheet, hal::format::Format::Rgba8Srgb)
                .with_data_width(reg.sprite_sheet_dimensions.0)
                .with_data_height(reg.sprite_sheet_dimensions.1)
                .with_sampler_info(SamplerDesc::new(
                    hal::image::Filter::Nearest,
                    hal::image::WrapMode::Tile,
                ))
                .with_view_kind(hal::image::ViewKind::D2)
                .build(
                    ImageState {
                        queue,
                        stage: hal::pso::PipelineStage::FRAGMENT_SHADER,
                        access: hal::image::Access::SHADER_READ,
                        layout: hal::image::Layout::ShaderReadOnlyOptimal,
                    },
                    factory,
                )
                .unwrap()
        };

        let descriptor_set = factory
            .create_descriptor_set(set_layouts[1].clone())
            .unwrap();

        unsafe {
            factory.device().write_descriptor_sets(vec![
                hal::pso::DescriptorSetWrite {
                    set: descriptor_set.raw(),
                    binding: 1,
                    array_offset: 0,
                    descriptors: vec![hal::pso::Descriptor::Image(
                        texture.view().raw(),
                        hal::image::Layout::ShaderReadOnlyOptimal,
                    )],
                },
                hal::pso::DescriptorSetWrite {
                    set: descriptor_set.raw(),
                    binding: 2,
                    array_offset: 0,
                    descriptors: vec![hal::pso::Descriptor::Sampler(texture.sampler().raw())],
                },
            ]);
        }

        Ok(ChunksGraphicsPipeline {
            chunks: HashMap::new(),
            texture,
            descriptor_set,
            uniform_buffer,
            sets,
            align,
        })
    }
}

impl<B: Backend> SimpleGraphicsPipeline<B, GameContext<B>> for ChunksGraphicsPipeline<B> {
    type Desc = ChunksGraphicsPipelineDesc;

    fn prepare(
        &mut self,
        factory: &Factory<B>,
        _queue: QueueId,
        _set_layouts: &[Handle<DescriptorSetLayout<B>>],
        index: usize,
        ctx: &GameContext<B>,
    ) -> PrepareResult {
        let dimensions = ctx.dimensions;
        let aspect_ratio = dimensions.0 as f32 / dimensions.1 as f32;
        let proj = Perspective3::new(aspect_ratio, std::f32::consts::FRAC_PI_4, 0.01, 100.0);
        let mut proj = proj.to_homogeneous();
        proj[5] = -proj[5];

        let view = ctx.camera.matrix();

        unsafe {
            factory
                .upload_visible_buffer(
                    &mut self.uniform_buffer,
                    uniform_offset(self.align, index as u64),
                    &[UniformArgs {
                        world: Matrix4::identity(),
                        view,
                        proj,
                    }],
                )
                .unwrap();
        }

        let mut chunks_to_regenerate = ctx.chunks_to_regenerate.lock().unwrap();

        if chunks_to_regenerate.len() > 0 {
            for chunk in chunks_to_regenerate.drain(..) {
                self.chunks
                    .insert(chunk, ChunkData::new(chunk, factory, ctx));
            }

            PrepareResult::DrawRecord
        } else {
            PrepareResult::DrawReuse
        }
    }

    fn draw(
        &mut self,
        layout: &<B as Backend>::PipelineLayout,
        mut encoder: RenderPassEncoder<'_, B>,
        index: usize,
        ctx: &GameContext<B>,
    ) {
        unsafe {
            encoder.bind_graphics_descriptor_sets(
                layout,
                0,
                vec![self.sets[index].raw(), self.descriptor_set.raw()],
                std::iter::empty::<u32>(),
            );

            for chunk in &ctx.chunks_to_render {
                if let Some(data) = self.chunks.get(chunk) {
                    encoder.bind_vertex_buffers(0, Some((data.vbuf.raw(), 0)));
                    encoder.bind_index_buffer(data.ibuf.raw(), 0, hal::IndexType::U32);

                    encoder.draw_indexed(0..data.inum as u32, 0, 0..1);
                }
            }
        }
    }

    fn dispose(self, _factory: &mut Factory<B>, _aux: &GameContext<B>) {}
}
