use crate::world::{registry::BLOCK_REGISTRY, BlockId, World};
use nalgebra::{Matrix4, Point3, Vector3};

pub trait Camera {
    fn matrix(&self) -> Matrix4<f32>;
}

pub struct FirstPersonCamera {
    pub(crate) pos: Point3<f32>,
    pub(crate) forward: Vector3<f32>,
    upwards: Vector3<f32>,
    yaw: f32,
    pitch: f32,
}

impl Default for FirstPersonCamera {
    fn default() -> Self {
        FirstPersonCamera::new()
    }
}

impl FirstPersonCamera {
    pub fn new() -> FirstPersonCamera {
        FirstPersonCamera {
            pos: [0.0, 0.0, 0.0].into(),
            forward: [0.0, 0.0, 1.0].into(),
            upwards: [0.0, 1.0, 0.0].into(),
            yaw: 90.0,
            pitch: 0.0,
        }
    }
}

impl Camera for FirstPersonCamera {
    fn matrix(&self) -> Matrix4<f32> {
        Matrix4::look_at_rh(
            &self.pos,
            &(self.pos + self.forward),
            &[0.0, 1.0, 0.0].into(),
        )
    }
}

const DEG_TO_RAD: f32 = std::f32::consts::PI / 180.0;

impl FirstPersonCamera {
    pub fn find_block(&self, world: &World) -> Option<(Vector3<i32>, BlockId)> {
        let registry = BLOCK_REGISTRY.lock().unwrap();
        let nothing_id = registry.get_id("core".to_string(), "nothing".to_string())?;

        let (mut x, mut y, mut z) = (self.pos.x as i32, self.pos.y as i32, self.pos.z as i32);
        let (bx, by, bz) = (self.pos.x % 1.0, self.pos.y % 1.0, self.pos.z % 1.0);

        let step_x = if self.forward.x.is_sign_negative() {
            -1
        } else {
            1
        };
        let step_y = if self.forward.y.is_sign_negative() {
            -1
        } else {
            1
        };
        let step_z = if self.forward.z.is_sign_negative() {
            -1
        } else {
            1
        };

        let out_x = x + 5 * step_x;
        let out_y = y + 5 * step_y;
        let out_z = z + 5 * step_z;

        let mut t_max_x = if self.forward.x == 0.0 {
            step_x as f32 * std::f32::INFINITY
        } else if step_x == 1 {
            (bx.ceil() - bx) / self.forward.x
        } else {
            (bx - bx.floor()) / self.forward.x
        };
        let mut t_max_y = if self.forward.y == 0.0 {
            step_y as f32 * std::f32::INFINITY
        } else if step_y == 1 {
            (by.ceil() - by) / self.forward.y
        } else {
            (by - by.floor()) / self.forward.y
        };
        let mut t_max_z = if self.forward.z == 0.0 {
            step_z as f32 * std::f32::INFINITY
        } else if step_z == 1 {
            (bz.ceil() - bz) / self.forward.z
        } else {
            (bz - bz.floor()) / self.forward.z
        };

        let t_delta_x = if self.forward.x == 0.0 {
            step_x as f32 * std::f32::INFINITY
        } else {
            1.0 / self.forward.x
        };
        let t_delta_y = if self.forward.y == 0.0 {
            step_y as f32 * std::f32::INFINITY
        } else {
            1.0 / self.forward.y
        };
        let t_delta_z = if self.forward.z == 0.0 {
            step_z as f32 * std::f32::INFINITY
        } else {
            1.0 / self.forward.z
        };

        let mut block = world.get_block(x, y, z)?;
        while block == nothing_id {
            if t_max_x < t_max_y {
                if t_max_x < t_max_z {
                    x += step_x;
                    if x == out_x {
                        return None;
                    }
                    t_max_x += t_delta_x;
                } else {
                    z += step_z;
                    if z == out_z {
                        return None;
                    }
                    t_max_z += t_delta_z;
                }
            } else if t_max_y < t_max_z {
                y += step_y;
                if y == out_y {
                    return None;
                }
                t_max_y += t_delta_y;
            } else {
                z += step_z;
                if z == out_z {
                    return None;
                }
                t_max_z += t_delta_z;
            }

            block = world.get_block(x, y, z)?;
        }

        Some(([x, y, z].into(), block))
    }

    pub(crate) fn handle_mouse(&mut self, x: f32, y: f32) {
        let sensitivity = 0.5;
        self.yaw += x * sensitivity;
        self.pitch += y * sensitivity;

        if self.pitch > 89.0 {
            self.pitch = 89.0;
        }
        if self.pitch < -89.0 {
            self.pitch = -89.0;
        }

        let forward: Vector3<f32> = [
            (self.yaw * DEG_TO_RAD).cos() * (self.pitch * DEG_TO_RAD).cos(),
            (self.pitch * DEG_TO_RAD).sin(),
            (self.yaw * DEG_TO_RAD).sin() * (self.pitch * DEG_TO_RAD).cos(),
        ]
        .into();

        // log::debug!("yaw: {}, pitch: {}", self.yaw, self.pitch);
        self.forward = forward.normalize();
    }

    pub(crate) fn move_forward(&mut self, by: f32) {
        let vec: Vector3<f32> = [self.forward.x, 0.0, self.forward.z].into();
        self.pos += vec.normalize() * by;
    }

    pub(crate) fn move_upwards(&mut self, by: f32) {
        let vec: Vector3<f32> = [0.0, by, 0.0].into();
        self.pos += vec;
    }

    pub(crate) fn move_sideways(&mut self, by: f32) {
        self.pos += self.forward.cross(&self.upwards).normalize() * by;
    }
}
