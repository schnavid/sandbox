use rendy::{
    hal::format::Format,
    mesh::{AsAttribute, AsVertex, Normal, Position, TexCoord, VertexFormat},
};
// use crate::graphics::pipelines::chunks::SHADER_REFLECTION;

#[derive(Debug, Copy, Clone, PartialOrd, PartialEq)]
pub struct TileUv(pub [f32; 2]);

impl AsAttribute for TileUv {
    const NAME: &'static str = "tile_uv";
    const FORMAT: Format = Format::Rg32Sfloat;
}

impl From<[f32; 2]> for TileUv {
    fn from(x: [f32; 2]) -> Self {
        TileUv(x)
    }
}

#[derive(Debug, Copy, Clone, PartialOrd, PartialEq)]
pub struct AmbientOcclusion(pub f32);

impl AsAttribute for AmbientOcclusion {
    const NAME: &'static str = "ambient_occlusion";
    const FORMAT: Format = Format::D32Sfloat;
}

impl From<f32> for AmbientOcclusion {
    fn from(x: f32) -> Self {
        AmbientOcclusion(x)
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct BlockVertex {
    position: Position,
    normal: Normal,
    tex_coord: TexCoord,
    tile_uv: TileUv,
    ambient_occlusion: AmbientOcclusion,
}

impl Default for BlockVertex {
    fn default() -> Self {
        BlockVertex {
            position: [0.0, 0.0, 0.0].into(),
            normal: [0.0, 0.0, 0.0].into(),
            tex_coord: [0.0, 0.0].into(),
            tile_uv: [0.0, 0.0].into(),
            ambient_occlusion: 0.0.into(),
        }
    }
}

impl BlockVertex {
    pub fn new(
        position: [f32; 3],
        normal: [f32; 3],
        tex_coord: [f32; 2],
        tile_uv: [f32; 2],
        ambient_occlusion: u32,
    ) -> BlockVertex {
        BlockVertex {
            position: position.into(),
            normal: normal.into(),
            tex_coord: tex_coord.into(),
            tile_uv: tile_uv.into(),
            ambient_occlusion: (ambient_occlusion as f32).into(),
        }
    }
}

impl AsVertex for BlockVertex {
    fn vertex() -> VertexFormat {
        VertexFormat::new((
            Position::vertex(),
            Normal::vertex(),
            TexCoord::vertex(),
            TileUv::vertex(),
            AmbientOcclusion::vertex(),
        ))
    }
}
